﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication6;

namespace WebApplication6.Controllers
{
    public class UserInRolesController : Controller
    {
        private HennuDemoEntities db = new HennuDemoEntities();

        // GET: UserInRoles
        public ActionResult Index()
        {
            var userInRoles = db.UserInRoles.Include(u => u.Role).Include(u => u.User);
            return View(userInRoles.ToList());
        }

        // GET: UserInRoles/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserInRole userInRole = db.UserInRoles.Find(id);
            if (userInRole == null)
            {
                return HttpNotFound();
            }
            return View(userInRole);
        }

        // GET: UserInRoles/Create
        public ActionResult Create()
        {
            ViewBag.RoleID = new SelectList(db.Roles, "RoleID", "RoleName");
            ViewBag.UserID = new SelectList(db.Users, "UserID", "Email");
            return View();
        }

        // POST: UserInRoles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,UserID,RoleID")] UserInRole userInRole)
        {
            if (ModelState.IsValid)
            {
                db.UserInRoles.Add(userInRole);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.RoleID = new SelectList(db.Roles, "RoleID", "RoleName", userInRole.RoleID);
            ViewBag.UserID = new SelectList(db.Users, "UserID", "Email", userInRole.UserID);
            return View(userInRole);
        }

        // GET: UserInRoles/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserInRole userInRole = db.UserInRoles.Find(id);
            if (userInRole == null)
            {
                return HttpNotFound();
            }
            ViewBag.RoleID = new SelectList(db.Roles, "RoleID", "RoleName", userInRole.RoleID);
            ViewBag.UserID = new SelectList(db.Users, "UserID", "Email", userInRole.UserID);
            return View(userInRole);
        }




        // POST: UserInRoles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,UserID,RoleID")] UserInRole userInRole)
        {
            if (ModelState.IsValid)
            {
                db.Entry(userInRole).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.RoleID = new SelectList(db.Roles, "RoleID", "RoleName", userInRole.RoleID);
            ViewBag.UserID = new SelectList(db.Users, "UserID", "Email", userInRole.UserID);
            return View(userInRole);
        }

        // GET: UserInRoles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserInRole userInRole = db.UserInRoles.Find(id);
            if (userInRole == null)
            {
                return HttpNotFound();
            }
            return View(userInRole);
        }

        // POST: UserInRoles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UserInRole userInRole = db.UserInRoles.Find(id);
            db.UserInRoles.Remove(userInRole);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

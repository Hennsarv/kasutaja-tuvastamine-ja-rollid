﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using W = WebApplication6;

namespace WebApplication6.Controllers
{
    public class HomeController : Controller
    {
        public void CheckUser()
        {
            if (Request.IsAuthenticated)
            {
                User u = W.User.GetByEmail(User.Identity.Name);
                if (u != null)
                {
                    Session["User"] = u;
                    ViewBag.AutenticatedUser = u;
                }
            }

        }

        public ActionResult Index()
        {
            CheckUser();
            return View();
        }

        public ActionResult About()
        {
            CheckUser();
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            CheckUser();
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
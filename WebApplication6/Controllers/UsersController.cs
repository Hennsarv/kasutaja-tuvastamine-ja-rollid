﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication6;
using W = WebApplication6;
using System.IO;
// midagi muudan

namespace WebApplication6
{
    partial class User
    {
        // ma teen siia mõned täiendused
        static HennuDemoEntities db = new HennuDemoEntities();
        // see on selle jaoks, et mul alati oleks igas meetodis andmebaas kasutada

        // staatiline meetod, mis leiab (kui on) User-i tema meili alusel
        public static User GetByEmail(string email)
            => db.Users.Where(x => x.Email == email).SingleOrDefault();

        public bool IsInRole(string roleNames)
            => this.UserInRoles    // vaatan kasutaja küljes olevaid rolle
                .Select(x => x.Role.RoleName.ToLower()) // leian iga rolli nime
                                                        //              .Contains(roleName.ToLower()); // kontrollin, kas küsitav roll on nende hulgas
                    .Intersect(roleNames
                .Split(',').Select(x => x.ToLower())).Count() > 0;
        public string FullName => $"{FirstName} {LastName}";

    }
}

namespace WebApplication6.Controllers
{
    public class UsersController : Controller
    {
        private HennuDemoEntities db = new HennuDemoEntities();


        // lisa omale IGA controlleri päisesse meetod
        // iga kontrolleri meetodi alguses kutsus see välja
        
        public void CheckUser()
        {
            if(Request.IsAuthenticated)
            {
                User u = W.User.GetByEmail(User.Identity.Name);
                if (u != null)
                {
                    Session["User"] = u;
                    ViewBag.AutenticatedUser = u;
                }
            }
            
        }

        // GET: Users
        public ActionResult Index()
        {
            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");

            // kas on meie kasutaja
            User u = W.User.GetByEmail(User.Identity.Name);
            if (u == null) return RedirectToAction("Index", "Home");

            ViewBag.CurrentUser = u;

            return View(db.Users.ToList());
        }

        // GET: Users/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }

            // siia pistan paar kontrolli:
            // 1. Kas kasutaja on sisse loginud ja meie kasutaja
            // 2. kas kasutaja kuulub õigesse rolli

            // kas on sisse loginud (või häkker)
            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");

            // kas on meie kasutaja
            User u = W.User.GetByEmail(User.Identity.Name);
            if (u == null) return RedirectToAction("Index", "Home");
            ViewBag.CurrentUser = u;
            ViewBag.Roles = 
            db.Roles.Where(r => ! r.UserInRoles
                                   .Select(y => y.UserID)
            .Contains(user.UserID)).ToList();

            return View(user);
        }

        // GET: Users/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "FirstName,LastName")] User user)
        {
            if (ModelState.IsValid)
            {
                user.Email = User.Identity.Name;

                db.Users.Add(user);
                db.SaveChanges();
                
            }

            return RedirectToAction("Index", "Home");
        }

        // GET: Users/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }

            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");

            // kas on meie kasutaja
            User u = W.User.GetByEmail(User.Identity.Name);
            if (u == null) return RedirectToAction("Index", "Home");

            if (u.IsInRole("Admin"))
                return View(user);
            if (u.UserID == user.UserID) return View(user);

            return RedirectToAction("Index");
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(
            [Bind(Include = "UserID,Email,FirstName,LastName")] User user, 
            HttpPostedFileBase file
            )
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                if (file != null)
                    if (file.ContentLength > 0)
                    { // lisame inimesele pildi
                        string[] filenams = file.FileName.Split('\\');

                        W.File f = new File();

                        f.Filename = filenams[filenams.Length - 1];

                        //if (f.Filetype?.Trim() == "")
                        {
                            string[] filetyps = file.FileName.Split('.');
                            if (filetyps.Length < 2) f.Filetype = "xxx";
                            else f.Filetype = filetyps[filetyps.Length - 1];
                        }
                        f.Contenttype = file.ContentType;

                        if (file.ContentLength > 0)
                        {
                            using (BinaryReader br = new BinaryReader(file.InputStream))
                            {
                                byte[] buff = br.ReadBytes(file.ContentLength);
                                f.Content = buff;
                            }
                            db.Files.Add(f);
                            db.SaveChanges();

                            user.PictureID = f.FileID;
                            db.SaveChanges();

                        }
                    }
                        return RedirectToAction("Index");
            }
            return View(user);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        public ActionResult AddRole(int? id)
        {
            if (!id.HasValue) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            int kasutajaId = id.Value / 1000;
            int rolliID = id.Value % 1000;

            db.UserInRoles.Add(new UserInRole { UserID = kasutajaId, RoleID = rolliID });
            db.SaveChanges();
            return RedirectToAction("Details", new { id = kasutajaId });
        }

        public ActionResult RemoveRole(int? id)
        {
            if (!id.HasValue) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            int kasutajaId = id.Value / 1000;
            int rolliID = id.Value % 1000;

            var ur = db.UserInRoles
                .Where(x => x.UserID == kasutajaId && x.RoleID == rolliID)
                .FirstOrDefault();
            if (ur != null)
            {
                db.UserInRoles.Remove(ur);
                db.SaveChanges();
            }

            db.SaveChanges();
            return RedirectToAction("Details", new { id = kasutajaId });
        }


        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.Users.Find(id);
            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}


-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 11/15/2018 15:03:48
-- Generated from EDMX file: C:\Users\sarvi\Source\Repos\kasutaja-tuvastamine-ja-rollid\WebApplication6\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [HennuDemo];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK__UserInRol__RoleI__76969D2E]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserInRole] DROP CONSTRAINT [FK__UserInRol__RoleI__76969D2E];
GO
IF OBJECT_ID(N'[dbo].[FK__UserInRol__UserI__75A278F5]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserInRole] DROP CONSTRAINT [FK__UserInRol__UserI__75A278F5];
GO
IF OBJECT_ID(N'[dbo].[FK__Users__PictureID__02FC7413]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Users] DROP CONSTRAINT [FK__Users__PictureID__02FC7413];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Files]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Files];
GO
IF OBJECT_ID(N'[dbo].[Person]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Person];
GO
IF OBJECT_ID(N'[dbo].[Roles]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Roles];
GO
IF OBJECT_ID(N'[dbo].[UserInRole]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UserInRole];
GO
IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Roles'
CREATE TABLE [dbo].[Roles] (
    [RoleID] int IDENTITY(1,1) NOT NULL,
    [RoleName] nvarchar(32)  NULL
);
GO

-- Creating table 'UserInRoles'
CREATE TABLE [dbo].[UserInRoles] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [UserID] int  NOT NULL,
    [RoleID] int  NOT NULL
);
GO

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [UserID] int IDENTITY(1,1) NOT NULL,
    [Email] nvarchar(128)  NOT NULL,
    [FirstName] nvarchar(32)  NULL,
    [LastName] nvarchar(32)  NULL,
    [PictureID] int  NULL
);
GO

-- Creating table 'People'
CREATE TABLE [dbo].[People] (
    [Code] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(25)  NULL
);
GO

-- Creating table 'Files'
CREATE TABLE [dbo].[Files] (
    [FileID] int IDENTITY(1,1) NOT NULL,
    [Filename] nvarchar(128)  NOT NULL,
    [Filetype] varchar(5)  NULL,
    [Contenttype] nvarchar(128)  NOT NULL,
    [Content] varbinary(max)  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [RoleID] in table 'Roles'
ALTER TABLE [dbo].[Roles]
ADD CONSTRAINT [PK_Roles]
    PRIMARY KEY CLUSTERED ([RoleID] ASC);
GO

-- Creating primary key on [ID] in table 'UserInRoles'
ALTER TABLE [dbo].[UserInRoles]
ADD CONSTRAINT [PK_UserInRoles]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [UserID] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([UserID] ASC);
GO

-- Creating primary key on [Code] in table 'People'
ALTER TABLE [dbo].[People]
ADD CONSTRAINT [PK_People]
    PRIMARY KEY CLUSTERED ([Code] ASC);
GO

-- Creating primary key on [FileID] in table 'Files'
ALTER TABLE [dbo].[Files]
ADD CONSTRAINT [PK_Files]
    PRIMARY KEY CLUSTERED ([FileID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [RoleID] in table 'UserInRoles'
ALTER TABLE [dbo].[UserInRoles]
ADD CONSTRAINT [FK__UserInRol__RoleI__76969D2E]
    FOREIGN KEY ([RoleID])
    REFERENCES [dbo].[Roles]
        ([RoleID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__UserInRol__RoleI__76969D2E'
CREATE INDEX [IX_FK__UserInRol__RoleI__76969D2E]
ON [dbo].[UserInRoles]
    ([RoleID]);
GO

-- Creating foreign key on [UserID] in table 'UserInRoles'
ALTER TABLE [dbo].[UserInRoles]
ADD CONSTRAINT [FK__UserInRol__UserI__75A278F5]
    FOREIGN KEY ([UserID])
    REFERENCES [dbo].[Users]
        ([UserID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__UserInRol__UserI__75A278F5'
CREATE INDEX [IX_FK__UserInRol__UserI__75A278F5]
ON [dbo].[UserInRoles]
    ([UserID]);
GO

-- Creating foreign key on [PictureID] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [FK__Users__PictureID__02FC7413]
    FOREIGN KEY ([PictureID])
    REFERENCES [dbo].[Files]
        ([FileID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__Users__PictureID__02FC7413'
CREATE INDEX [IX_FK__Users__PictureID__02FC7413]
ON [dbo].[Users]
    ([PictureID]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------
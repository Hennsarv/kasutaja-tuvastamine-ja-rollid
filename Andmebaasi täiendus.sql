﻿-- natuke hiina keelt
-- ma teen tabeli, kus hakkan hoidma faile (igasuguseid)

create table Files
(
	FileID int identity primary key, -- igal failil üks sisemine number
	[Filename] sysname not null, -- igal failil miski nimi (pärast vaatame, kust selle saab)
	Filetype varchar(5) null, -- failitüüp - see on sealt nimesabast
	Contenttype varchar(32) null, -- sisutüüp (ka pisut vajalik asi)
	Content varbinary(max) -- lõpuks siis faili sisu
)

-- üks võimalus kus seda faili kasutada
-- näiteks lisan Users tabelile koha, kus ütelda
-- millise numbrid gail on tema pildi kandja
alter table Users
add PictureID int null references Files(fileID)

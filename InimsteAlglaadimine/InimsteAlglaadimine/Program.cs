﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InimsteAlglaadimine
{
    class Program
    {
        static void Main(string[] args)
        {
            HennuDemoEntities db = new HennuDemoEntities();
            var userEmails = db.Users.Select(x => x.Email).ToList();

            string lapsed = @"..\..\Lapsed.txt";
            var loetud = System.IO.File.ReadAllLines(lapsed)
                .Select(x => x.Split(','))
                .Where(x => !userEmails.Contains(x[2]))
                .Select(x => new User { FirstName = x[0], LastName = x[1], Email = x[2] })
                .ToList()
                ;
            db.Users.AddRange(loetud);
            db.SaveChanges();

            // see lõik käib kõik lisatud User-id läbi ja lisab neile rolli "Kasutaja"

            // otsime välja rolli
            var uusroll = db.Roles
                .Where(x => x.RoleName == "Kasutaja")
                .SingleOrDefault();
            if (uusroll != null) // kui see olemas, lisame kasutajatele
            {
                foreach(var u in loetud)
                db.UserInRoles.Add(new UserInRole
                { Role = uusroll, User = u }
                );
            }
            db.SaveChanges();

            // teeme sama rolliga autojuht, aga pisut teisiti
            uusroll = db.Roles
                .Where(x => x.RoleName == "Autojuht")
                .SingleOrDefault();
            if (uusroll != null)
            {
                loetud.ForEach(
                                x => x.UserInRoles.Add(
                                    new UserInRole { Role = uusroll }
                                )
                              );
            }
            db.SaveChanges();
        }
    }
}
